using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BattleState {START, PLAYERTURN, ENEMYTURN, WON, LOSE };
public class BattleSystem : MonoBehaviour
{
    public GameObject playerPrefab;
    public List<GameObject> enemiesPrefab;

    public BattleState state;

    CardManager cardManager;

    void Start()
    {
        state = BattleState.START;
    }

    void SetupBattle()
    {
        //set up deck
        //draw card
    }

    void PlayerTurn()
    {
         //set player's mana cost
    }

    void EnemyTurn()
    {
        // enemy action
    }
}

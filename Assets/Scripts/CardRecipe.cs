using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
public class CardRecipe : MonoBehaviour,IDragHandler,IBeginDragHandler,IEndDragHandler
{
    public Action onMouseDrag;
    public Action onMouseDown;
    public Action onMouseDrop;

    public List<GameObject> craftCards;

    Vector3 currentPoint;
    Transform currentParent;
    // Start is called before the first frame update
    void Start()
    {
        onMouseDrag += MovingCard;
    }

    void MovingCard()
    {
        transform.position = Input.mousePosition;
        transform.SetParent(CardManager.instance.selectCanvas.transform);
    }

    void CraftRecipe()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        currentPoint = transform.position;
        currentParent = transform.parent;
    }

    public void OnDrag(PointerEventData eventData)
    {
        onMouseDrag?.Invoke();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = currentPoint;
        transform.parent = currentParent;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class CardIngredient : MonoBehaviour, IDragHandler,IBeginDragHandler,IEndDragHandler
{
    public Action onMouseDown;
    public Action onMouseDrag;
    public Action onMouseDrop;

    Vector3 currentPoint;
    Transform currentParent;
    // Start is called before the first frame update
    void Start()
    {
        onMouseDrag += MovingCard;
    }

    void MovingCard()
    {
        transform.position = Input.mousePosition;
        transform.SetParent(CardManager.instance.selectCanvas.transform);
    }

    // Update is called once per frame
    void Update()
    {

    }
        public void OnBeginDrag(PointerEventData eventData)
    {
        currentPoint = transform.position;
        currentParent = transform.parent;
    }

    public void OnDrag(PointerEventData eventData)
    {
        onMouseDrag?.Invoke();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = currentPoint;
        transform.parent = currentParent;
    }

}

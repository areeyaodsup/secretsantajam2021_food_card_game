using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public List<Wave> _waves = new List<Wave>();
    // Start is called before the first frame update
    void Start()
    {
        InitWave(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitWave(int wave)
    {
        float distance_per_unit = 16.0f / (_waves[wave]._enemys.Count+1);
        float pos = 0;
        
        print("Count :" + _waves[wave]._enemys.Count);
        print("Distance :" + distance_per_unit);
        for (int i = 0; i < _waves[wave]._enemys.Count; i++)
        {
            pos += distance_per_unit;
            GameObject enemy = Instantiate(_waves[wave]._enemys[i].gameObject, new Vector3(pos, 0, 0),Quaternion.identity);
            
        }
    }
}
[System.Serializable]
public class Wave
{
    public List<Enemy> _enemys = new List<Enemy>();
}


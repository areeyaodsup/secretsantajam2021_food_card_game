using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum CardType { INGREDIENT, RECIPE };

[CreateAssetMenu(fileName ="New Card", menuName ="Cards")]
public class Card : ScriptableObject
{
    public new string name;
    public string description;

    public Sprite artwork;

    public int cost;
    public string rarity;
    public string type;
    public CardType cardtype;
 }

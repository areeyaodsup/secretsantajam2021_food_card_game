using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int _hp
    {
        get
        {
            return m_hp;
        }
        set
        {
            m_hp = value;
        }
    }
    public int m_hp;

    public int _damage;
}

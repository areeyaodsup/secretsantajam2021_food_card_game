using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    public List<GameObject> cardPrefabs;
    public GameObject canvas;
    public GameObject selectCanvas;

   [SerializeField]
    private List<Card> deck;
    int handCardLimit = 5;
    //int deckCards = 20;

    public static CardManager instance;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        CreateCard();
        //starter deck?
        //create card 
        //pick up card
    }
    
    void CreateCard()
    {
       for(int i = 0; i < cardPrefabs.Count; i++)
        {
            GameObject newCard = Instantiate(cardPrefabs[i]);
            newCard.transform.SetParent(canvas.transform);
            newCard.transform.localScale = new Vector3(1, 1, 1);
            newCard.transform.localPosition = Vector2.zero;
        }
    }

    // Update is called once per frame
    void Update()
    {
      
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CardDisplay : MonoBehaviour
{
    public Card card;

    public TMP_Text nameOfCard;
    public TMP_Text cost;
    public TMP_Text description;

    public TMP_Text type;
    public Image artwork;

    // Start is called before the first frame update
    void Start()
    {
        if (card == null)
            return;

        if (card.artwork != null)
            artwork.sprite = card.artwork;

        nameOfCard.text = card.name;
        description.text = card.description;
        cost.text = card.cost.ToString();
        type.text = card.type;

    }

}
